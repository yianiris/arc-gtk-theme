# Obarun        : 66 init/supervisor
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Obarun PkgSrc : url="https://framagit.org/pkg/obcommunity/arc-gtk-theme"
#----------------
# Maintainer    :
# Contributor   :
# Arch PkgSrc   : url="https://www.archlinux.org/packages/community/any/arc-gtk-theme/"
#----------------
# Website       : https://github.com/jnsh/arc-theme
#----------------------------------------------------------------------------
#--DESCRIPTION---------------------------------------------------------------

pkgbase=arc-gtk-theme

_pkgname=arc-theme
pkgname=('arc-gtk-theme' 'arc-solid-gtk-theme')

pkgdesc="A flat theme with transparent elements for GTK 3, GTK 2 and Gnome-Shell"

pkgver=20200819
pkgrel=2

url="https://github.com/jnsh/arc-theme"

track=   ## options: none             | tag         | branch      | commit
target="${_pkgname}-${pkgver}.tar.xz"  ## options: $pkgname-$pkgver | tag_version | branch_name | commit_id
source=("${pkgname}-${pkgver}.tar.xz::${url}/releases/download/${pkgver}/${target}")

#--BUILD CONFIGURATION-------------------------------------------------------

makedepends=(
	'sassc'
	'optipng'
	'inkscape')

checkdepends=()

#--BUILD PREPARATION---------------------------------------------------------

prepare() {
    cp -a ${_pkgname}-${pkgver}{,-solid}
}

#--BUILD---------------------------------------------------------------------

build() {
    cd ${_pkgname}-${pkgver}
	
	./autogen.sh --prefix=/usr --with-gnome-shell=3.36 --with-cinnamon=4.6 --with-gtk3=3.24
	
	cd ../${_pkgname}-${pkgver}-solid
    ./autogen.sh --prefix=/usr --disable-transparency --with-gnome-shell=3.36 --with-cinnamon=4.6 --with-gtk3=3.24
}

#--PACKAGE-------------------------------------------------------------------

package_arc-gtk-theme() {
    replaces=('gtk-theme-arc')

    cd ${_pkgname}-${pkgver}
    make DESTDIR="${pkgdir}" install
}

package_arc-solid-gtk-theme() {
    pkgdesc="A flat theme for GTK 3, GTK 2 and Gnome-Shell (without transparency)"
    replaces=('gtk-theme-arc-solid')

    cd ${_pkgname}-${pkgver}-solid
    make DESTDIR="${pkgdir}" install
}


#--INSTALL CONFIGURATION-----------------------------------------------------

arch=('x86_64')

optdepends=('arc-icon-theme: recommended icon theme'
            'gtk-engine-murrine: for gtk2 themes'
            'gnome-themes-standard: for gtk2 themes' )

#--SECURITY AND LICENCE------------------------------------------------------

sha512sums=('ac814babf995facce72497efad7d7afb6d2eed0ad02640aeb694874681c961b136fcc4fba8350fdee715d44c26db2a336b3430c054a299285ced782ed67bbcfe')

license=('GPL3')
